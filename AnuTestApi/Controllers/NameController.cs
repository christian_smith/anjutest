﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AnjuTestApi.Controllers
{
    public class NameController : ApiController
    {
        // GET api/name
        public IHttpActionResult Get()
        {
            return Ok(new { firstname = "John", lastname = "Doe" });
        }

        // GET api/name/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/name
        public void Post([FromBody] string value)
        {
        }

        // PUT api/name/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/name/5
        public void Delete(int id)
        {
        }
    }
}
